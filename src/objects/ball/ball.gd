extends ColorRect

var speed_x = 8.0
var speed_y = 8
var active = false
var bounce = true
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if Global.round % 2 == 1:
		speed_x = -speed_x 
	randomize()
	speed_y = randi_range(-speed_y,speed_y)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if !active:
		return
		
	position.x += speed_x
	position.y += speed_y
	pass


func _on_area_2d_area_entered(area: Area2D) -> void:
	speed_x += .01
	
	if area.is_in_group("walls") && bounce:
		bounce = false
		$bounceTimer.start()
		speed_y = speed_y * -1
		
		
	if area.is_in_group("players"):
		speed_x = speed_x * -1
		speed_y = (speed_y + area.get_parent().motion) -1


func _on_start_timer_timeout() -> void:
	active = true


func _on_end_timer_timeout() -> void:
	get_tree().reload_current_scene()


func _on_visible_on_screen_notifier_2d_screen_exited() -> void:
	Global.round += 1
	if speed_x > 0:
		Global.score[0] = Global.score[0] + 1
	else:
		Global.score[1] = Global.score[1] + 1
	$endTimer.start()


func _on_bounce_timer_timeout() -> void:
	bounce = true
