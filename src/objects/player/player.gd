extends ColorRect

@export var player_id = 0
var speed = 10
var motion = 1.0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if !Input.is_joy_known(player_id):
		return
	
	if Input.is_action_pressed("player_up") && position.y > 40:
		position.y -= speed
		motion = -speed / 3
	elif Input.is_action_pressed("player_down") && position.y < 430:
		position.y += speed 
		motion = speed / 3
		
	if motion > 1:
		motion = motion - delta
	
