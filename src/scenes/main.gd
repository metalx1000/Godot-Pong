extends Node2D

var balls
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	balls = get_tree().get_nodes_in_group("balls")
	$score.text = str(Global.score[0]) + ":" + str(Global.score[1])


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if Input.is_action_just_pressed("quit"):
		get_tree().quit()
	
